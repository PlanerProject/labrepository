﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planer
{
   public class Person 
    {

	    //ФИО
        private	string surname;
        private string name;
	    private string patronymic;
	    //дата рождения
	    private DateTime date;

        protected Person(string  name_ = "\0", 
		             string  surname_ = "\0",
			         string  patronymic_ ="\0",
			         int day= -1, int month = -1, int year = -1)
        {
            date = new DateTime(year,month,day);
            this.name = name_;
            this.surname = surname_;
            this.patronymic = patronymic_;
            
        }

	 

	    public string  getSurname()
        {
            return this.surname;
        }
	    public string  getName()
        {
            return this.name;
        }
	    public string  getPatronymic()
        {
            return this.patronymic;
        }
	    public DateTime  getBirthdayStringF()
        {
            return this.date;
        }
        public int getDayMonthBirth()
        {
            return this.date.Day;
        }
	    public int getMonthBirth()
        {
            return this.date.Month;
        }

	    void setSurname(string sursName_)
        {
            this.surname = sursName_;
        }
	    void setName(string name_)
        {
            this.name = name_; 
        }
	    void setPatronymic(string patronymic_)
        {
            this.patronymic = patronymic_;
        }

        void addBirthday(int day, int month, int year = -1)
        {
            this.date.AddDays(day);
            this.date.AddYears(year);
            this.date.AddMonths(month);
        }
    }
}
