﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planer
{
    public class Contact : Person
    {
	    private string phoneNumber;
	    private string email;
	    private string address;
        private string information;
	    public Contact( string name_ , 
		                string surname_ = "\0",
			            string patronymic_ = "\0",
			            string phoneNumber_ = "\0",
			            string email_ = "\0",
			            string address_ ="\0",
			            int day_ = -1,
			            int month_ = -1,
			            int year_ = -1,
		                string information_ = "\0")
            : base(name_, surname_, patronymic_, day_, month_, year_)
        {
            this.phoneNumber = phoneNumber_;
	        this.email = email_;
	        this.address = address_;
            this.information = information_;
                        
        }
	    public string getPhoneNumber()
        {
            return   this.phoneNumber;
        }
	    public string getEmail()
        {
            return this.email;
        }
	    void  setPhoneNumber(string phoneNumber_)
        {
            this.phoneNumber = phoneNumber_;
        }
	    void  setEmail(string email_)
        {
            this.email = email_;
        }
  }
   
}
    