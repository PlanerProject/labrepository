﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Planer
{
    public partial class Zam : Form
    {
        Note temp;
        Note temp2;
        public Zam(Note note_)
        {
            this.temp = note_;
            this.temp2 = note_;
            temp2.setText(note_.getText());
            temp2.setTheme(note_.getTheme());
            InitializeComponent();
            this.richTextBox1.Text = note_.getText();
            this.textBox1.Text = note_.getTheme();
        }   
        

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//Кнопка ОК и проверка если вносились изменения
        {
            //string tempText = temp2.getText();
            if (temp2.getText() == richTextBox1.Text & temp2.getTheme() == textBox1.Text)
            {
                this.Close();
            }
            else
            {
                DialogResult result1 = MessageBox.Show("Сохранить?",
                    "Сохранить изменения?",
                    MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    temp.setText(richTextBox1.Text);
                    temp.setTheme(textBox1.Text);
                    this.Close();
                }
                else
                {
                    this.Close();
                }
            }
                // return savenote_;
            
        }

        private void заметкиBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.заметкиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.db_planerDataSet);
            
        }

        private void Zam_Load(object sender, EventArgs e)
        {


        }

        private void noteTextBox_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void noteLabel_Click(object sender, EventArgs e)
        {

        }
        public Note note_ { get; set; }

        

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)//Кнопка Отмена
        {
            this.Close();
        }
    }
}
