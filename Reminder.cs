﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planer
{
    public class Reminder 
    {
        private string text;
        private DateTime date;
        public Reminder(string text, DateTime date)        
        {
            this.date = date;
            this.text = text;
        }
        public DateTime getDateTime()
        {
            return this.date;
           
        }
        public void setDateTime(DateTime date)
        {
            this.date = date;
        }
        public void setText(string text)
        {
            this.text = text;
        }
        public string getText()
        {
            return this.text;
        }
    }
}
