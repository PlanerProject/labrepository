﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planer
{
    public class Note
    {
        private string theme;
        private string text;

        public Note(string theme = "" , string text = "")
        {
            this.theme = theme;
            this.text = text;
        }

        public void setText(string text)
        {
            this.text = text;        
        }
        public void setTheme(string theme)
        {
            this.theme = theme;
        }
        public string getText()
        {
            return this.text;
        }
        public string getTheme()
        {
            return this.theme;
        }
        public void clearNote()
        {
            this.theme = "";
            this.text  = "";
        }
    }
}
