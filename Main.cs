﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Planer
{ 
    
    public partial class Main : Form
    {
        Note note_;
        Reminder remind_;
        public Main()
        {
            
            InitializeComponent();
            this.timer1.Enabled = true;
            
                  
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void заметкиBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.заметкиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.db_planerDataSet);

        }

        private void Main_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "db_planerDataSet.Контакты". При необходимости она может быть перемещена или удалена.
            this.контактыTableAdapter.Fill(this.db_planerDataSet.Контакты);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "db_planerDataSet.Напоминания". При необходимости она может быть перемещена или удалена.
            this.напоминанияTableAdapter.Fill(this.db_planerDataSet.Напоминания);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "db_planerDataSet.Заметки". При необходимости она может быть перемещена или удалена.
            this.заметкиTableAdapter.Fill(this.db_planerDataSet.Заметки);

        }

        private void заметкиDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            Note note_ = new Note (заметкиDataGridView.CurrentRow.Cells[1].Value.ToString(), заметкиDataGridView.CurrentCell.Value.ToString());

            Zam f2 = new Zam (note_);
            f2.ShowDialog();
           
            заметкиDataGridView.CurrentRow.Cells[2].Value = note_.getText();
            заметкиDataGridView.CurrentRow.Cells[1].Value = note_.getTheme();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            заметкиDataGridView.CurrentRow.Cells[2].Value = note_.getText();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {

        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e) //Меню Сохранить
        {
            this.Validate();
            this.заметкиBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.db_planerDataSet);
            this.timer1.Start();
        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e) //Меню Выйти
        {
            this.Close();
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e) //Меню добавить заметку
        {
            this.Validate();
            this.заметкиBindingSource.AddNew();
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e) //Меню добавить напоминание
        {
            this.timer1.Stop();
            this.Validate();
            this.напоминанияBindingSource.AddNew();
                      
           
        }
        
        private void добавитьКонтактToolStripMenuItem_Click(object sender, EventArgs e) //Меню добавить контакт
        {
            this.Validate();
            this.контактыBindingSource.AddNew();
        }

        private void удалитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.заметкиBindingSource.RemoveCurrent();

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.контактыBindingSource.RemoveCurrent();
        }

        private void удалитьНапоминаниеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.напоминанияBindingSource.RemoveCurrent();
        }
        
        private void timer1_Tick(object sender, EventArgs e)  //работа Таймера для часов и Напоминания.
        {
            label1.Text = DateTime.Now.ToString("hh:mm:ss"); //вывод часов
            this.timer1.Interval = 1000;
            for (int i = 0; i < напоминанияDataGridView.Rows.Count; i++) //Цикл (пробегаемся по базе)
            {
                if (напоминанияDataGridView[1, i].Value != null)
                {
                    string temp = напоминанияDataGridView[1, i].Value.ToString();

                    if (temp == DateTime.Now.ToString())
                    {
                        MessageBox.Show("Напоминание!!!");
                    }
                }
               
            }

        }

       
        private void напоминанияDataGridView_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            string date_str = напоминанияDataGridView.CurrentRow.Cells[1].Value.ToString();
            
            DateTime temp_date;
            temp_date = Convert.ToDateTime(date_str);

            Reminder rem_ = new Reminder(напоминанияDataGridView.CurrentRow.Cells[0].Value.ToString(),temp_date);
            Napom n2 = new Napom (rem_);
            n2.ShowDialog();

            напоминанияDataGridView.CurrentRow.Cells[0].Value = rem_.getText();
            напоминанияDataGridView.CurrentRow.Cells[1].Value = rem_.getDateTime();

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

       
    }
}
