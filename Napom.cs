﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Planer
{
    public partial class Napom : Form
    {
        Reminder temp;
        Reminder temp2;
        public Napom ( Reminder rem_)
        {
            this.temp = rem_;
            this.temp2 = rem_;
            InitializeComponent();
            textBox1.Text = rem_.getText();
            string date_temp = Convert.ToString(rem_.getDateTime());
            textBox2.Text = date_temp;
            temp2.setText(rem_.getText());
            temp2.setDateTime(rem_.getDateTime());

        }
        public Reminder rem_ { set; get; }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (temp2.getText() == textBox1.Text & temp2.getDateTime() == Convert.ToDateTime(textBox2.Text))
            {
                this.Close();
            }
            else
            {
                DialogResult result1 = MessageBox.Show("Сохранить?",
                    "Сохранить изменения?",
                    MessageBoxButtons.YesNo);
                if (result1 == DialogResult.Yes)
                {
                    temp.setText(textBox1.Text);
                    temp.setDateTime(Convert.ToDateTime(textBox2.Text));
                    this.Close();
                }
                else
                {
                    this.Close();
                }
            }
        }
    }
}
