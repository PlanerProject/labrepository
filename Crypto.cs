﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ODB = System.Data.OleDb;
using System.Data.OleDb;

namespace Planer
{
    class Crypto
    {
        static int counter;
        private string login;
        private string password;
        private bool locK;      // доступ к базе 
        private bool create;    // регистрация нового пользователя 

        public Crypto(string login_, string password_, bool create_ = false)
        {
            ++counter;
            this.login = login_;
            this.password = password_;
            this.create = create_;
            if (create_)
                this.inBase();
            else
                this.outBase();
        }
        ~Crypto()
        {
            --counter;
        }
        private void inBase()
        {
          // this.login;
   
        }
        private void outBase()
        {

        }

        public string getLogin()
        {
            return this.login;
        }

        public bool getCreate()
        {
            return this.create;
        }
        public bool getLock()
        {
            return this.locK;
        }


        //
        public void setLogin(string login)
        {
            this.login = login;
        }

        public void setPassword(string password)
        {
            this.password = password;
        }

        public void setCreate(bool create = false)
        {
            this.create = create;
            if (this.create)
                this.inBase();
        }
        //
        public static int getCounter()
        {
            return Crypto.counter;
        }

    }
}
