﻿namespace Planer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageZam = new System.Windows.Forms.TabPage();
            this.заметкиDataGridView = new System.Windows.Forms.DataGridView();
            this.tabPageNap = new System.Windows.Forms.TabPage();
            this.tabPageCont = new System.Windows.Forms.TabPage();
            this.контактыDataGridView = new System.Windows.Forms.DataGridView();
            this.UserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выйтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.правкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьКонтактToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьНапоминаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.заметкиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.db_planerDataSet = new Planer.db_planerDataSet();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.контактыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.напоминанияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.заметкиTableAdapter = new Planer.db_planerDataSetTableAdapters.ЗаметкиTableAdapter();
            this.tableAdapterManager = new Planer.db_planerDataSetTableAdapters.TableAdapterManager();
            this.контактыTableAdapter = new Planer.db_planerDataSetTableAdapters.КонтактыTableAdapter();
            this.напоминанияTableAdapter = new Planer.db_planerDataSetTableAdapters.НапоминанияTableAdapter();
            this.напоминанияDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPageZam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.заметкиDataGridView)).BeginInit();
            this.tabPageNap.SuspendLayout();
            this.tabPageCont.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.контактыDataGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.заметкиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_planerDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.контактыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.напоминанияBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.напоминанияDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(7, 149);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageZam);
            this.tabControl1.Controls.Add(this.tabPageNap);
            this.tabControl1.Controls.Add(this.tabPageCont);
            this.tabControl1.Location = new System.Drawing.Point(183, 54);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(498, 257);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageZam
            // 
            this.tabPageZam.AutoScroll = true;
            this.tabPageZam.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageZam.Controls.Add(this.заметкиDataGridView);
            this.tabPageZam.Location = new System.Drawing.Point(4, 22);
            this.tabPageZam.Name = "tabPageZam";
            this.tabPageZam.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageZam.Size = new System.Drawing.Size(490, 231);
            this.tabPageZam.TabIndex = 0;
            this.tabPageZam.Text = "Заметки";
            // 
            // заметкиDataGridView
            // 
            this.заметкиDataGridView.AutoGenerateColumns = false;
            this.заметкиDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.заметкиDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.заметкиDataGridView.DataSource = this.заметкиBindingSource;
            this.заметкиDataGridView.Location = new System.Drawing.Point(0, 0);
            this.заметкиDataGridView.Name = "заметкиDataGridView";
            this.заметкиDataGridView.Size = new System.Drawing.Size(484, 231);
            this.заметкиDataGridView.TabIndex = 0;
            this.заметкиDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.заметкиDataGridView_CellContentClick);
            // 
            // tabPageNap
            // 
            this.tabPageNap.AutoScroll = true;
            this.tabPageNap.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageNap.Controls.Add(this.напоминанияDataGridView);
            this.tabPageNap.Location = new System.Drawing.Point(4, 22);
            this.tabPageNap.Name = "tabPageNap";
            this.tabPageNap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNap.Size = new System.Drawing.Size(490, 231);
            this.tabPageNap.TabIndex = 1;
            this.tabPageNap.Text = "Напоминания";
            // 
            // tabPageCont
            // 
            this.tabPageCont.AutoScroll = true;
            this.tabPageCont.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPageCont.Controls.Add(this.контактыDataGridView);
            this.tabPageCont.Location = new System.Drawing.Point(4, 22);
            this.tabPageCont.Name = "tabPageCont";
            this.tabPageCont.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCont.Size = new System.Drawing.Size(490, 231);
            this.tabPageCont.TabIndex = 2;
            this.tabPageCont.Text = "Контакты";
            // 
            // контактыDataGridView
            // 
            this.контактыDataGridView.AutoGenerateColumns = false;
            this.контактыDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.контактыDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.UserID});
            this.контактыDataGridView.DataSource = this.контактыBindingSource;
            this.контактыDataGridView.Location = new System.Drawing.Point(0, 0);
            this.контактыDataGridView.Name = "контактыDataGridView";
            this.контактыDataGridView.Size = new System.Drawing.Size(803, 243);
            this.контактыDataGridView.TabIndex = 0;
            // 
            // UserID
            // 
            this.UserID.DataPropertyName = "UserID";
            this.UserID.HeaderText = "UserID";
            this.UserID.Name = "UserID";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.правкаToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(681, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem,
            this.выйтиToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem1.Text = "Файл";
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // выйтиToolStripMenuItem
            // 
            this.выйтиToolStripMenuItem.Name = "выйтиToolStripMenuItem";
            this.выйтиToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выйтиToolStripMenuItem.Text = "Выйти";
            this.выйтиToolStripMenuItem.Click += new System.EventHandler(this.выйтиToolStripMenuItem_Click);
            // 
            // правкаToolStripMenuItem
            // 
            this.правкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.удалитьToolStripMenuItem,
            this.добавитьКонтактToolStripMenuItem,
            this.удалитьToolStripMenuItem1,
            this.удалитьНапоминаниеToolStripMenuItem,
            this.toolStripMenuItem3});
            this.правкаToolStripMenuItem.Name = "правкаToolStripMenuItem";
            this.правкаToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.правкаToolStripMenuItem.Text = "Правка";
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.добавитьToolStripMenuItem.Text = "Добавить заметку";
            this.добавитьToolStripMenuItem.Click += new System.EventHandler(this.добавитьToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.удалитьToolStripMenuItem.Text = "Добавить напоминание";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // добавитьКонтактToolStripMenuItem
            // 
            this.добавитьКонтактToolStripMenuItem.Name = "добавитьКонтактToolStripMenuItem";
            this.добавитьКонтактToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.добавитьКонтактToolStripMenuItem.Text = "Добавить контакт";
            this.добавитьКонтактToolStripMenuItem.Click += new System.EventHandler(this.добавитьКонтактToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem1
            // 
            this.удалитьToolStripMenuItem1.Name = "удалитьToolStripMenuItem1";
            this.удалитьToolStripMenuItem1.Size = new System.Drawing.Size(205, 22);
            this.удалитьToolStripMenuItem1.Text = "Удалить заметку";
            this.удалитьToolStripMenuItem1.Click += new System.EventHandler(this.удалитьToolStripMenuItem1_Click);
            // 
            // удалитьНапоминаниеToolStripMenuItem
            // 
            this.удалитьНапоминаниеToolStripMenuItem.Name = "удалитьНапоминаниеToolStripMenuItem";
            this.удалитьНапоминаниеToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.удалитьНапоминаниеToolStripMenuItem.Text = "Удалить напоминание";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(205, 22);
            this.toolStripMenuItem3.Text = "Удалить контакт";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Desktop;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.LimeGreen;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 37);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NoteID";
            this.dataGridViewTextBoxColumn1.HeaderText = "NoteID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NoteName";
            this.dataGridViewTextBoxColumn2.HeaderText = "Тема";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Note";
            this.dataGridViewTextBoxColumn3.HeaderText = "Текст";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 380;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DateCreate";
            this.dataGridViewTextBoxColumn4.HeaderText = "DateCreate";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "UserID";
            this.dataGridViewTextBoxColumn5.HeaderText = "UserID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // заметкиBindingSource
            // 
            this.заметкиBindingSource.DataMember = "Заметки";
            this.заметкиBindingSource.DataSource = this.db_planerDataSet;
            // 
            // db_planerDataSet
            // 
            this.db_planerDataSet.DataSetName = "db_planerDataSet";
            this.db_planerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "FirstName";
            this.dataGridViewTextBoxColumn13.HeaderText = "Имя";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "LastName";
            this.dataGridViewTextBoxColumn14.HeaderText = "Фамилия";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Phone";
            this.dataGridViewTextBoxColumn15.HeaderText = "Телефон";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Address";
            this.dataGridViewTextBoxColumn16.HeaderText = "Адрес";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "E-mail";
            this.dataGridViewTextBoxColumn17.HeaderText = "E-mail";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "NotePhone";
            this.dataGridViewTextBoxColumn18.HeaderText = "Информация";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // контактыBindingSource
            // 
            this.контактыBindingSource.DataMember = "Контакты";
            this.контактыBindingSource.DataSource = this.db_planerDataSet;
            // 
            // напоминанияBindingSource
            // 
            this.напоминанияBindingSource.DataMember = "Напоминания";
            this.напоминанияBindingSource.DataSource = this.db_planerDataSet;
            // 
            // заметкиTableAdapter
            // 
            this.заметкиTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = Planer.db_planerDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.ЗаметкиTableAdapter = this.заметкиTableAdapter;
            this.tableAdapterManager.КонтактыTableAdapter = this.контактыTableAdapter;
            this.tableAdapterManager.НапоминанияTableAdapter = this.напоминанияTableAdapter;
            this.tableAdapterManager.ПользовательTableAdapter = null;
            // 
            // контактыTableAdapter
            // 
            this.контактыTableAdapter.ClearBeforeFill = true;
            // 
            // напоминанияTableAdapter
            // 
            this.напоминанияTableAdapter.ClearBeforeFill = true;
            // 
            // напоминанияDataGridView
            // 
            this.напоминанияDataGridView.AutoGenerateColumns = false;
            this.напоминанияDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.напоминанияDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.напоминанияDataGridView.DataSource = this.напоминанияBindingSource;
            this.напоминанияDataGridView.Location = new System.Drawing.Point(0, 0);
            this.напоминанияDataGridView.Name = "напоминанияDataGridView";
            this.напоминанияDataGridView.Size = new System.Drawing.Size(494, 231);
            this.напоминанияDataGridView.TabIndex = 0;
            this.напоминанияDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.напоминанияDataGridView_CellContentClick_1);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "RingName";
            this.dataGridViewTextBoxColumn9.HeaderText = "Напоминание";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 200;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DateRing";
            this.dataGridViewTextBoxColumn10.HeaderText = "Дата и время";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 200;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(481, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(681, 319);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Планировщик";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageZam.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.заметкиDataGridView)).EndInit();
            this.tabPageNap.ResumeLayout(false);
            this.tabPageCont.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.контактыDataGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.заметкиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.db_planerDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.контактыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.напоминанияBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.напоминанияDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageZam;
        private System.Windows.Forms.TabPage tabPageNap;
        private System.Windows.Forms.TabPage tabPageCont;
        private db_planerDataSet db_planerDataSet;
        private System.Windows.Forms.BindingSource заметкиBindingSource;
        private db_planerDataSetTableAdapters.ЗаметкиTableAdapter заметкиTableAdapter;
        private db_planerDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView заметкиDataGridView;
        private db_planerDataSetTableAdapters.НапоминанияTableAdapter напоминанияTableAdapter;
        private System.Windows.Forms.BindingSource напоминанияBindingSource;
        private db_planerDataSetTableAdapters.КонтактыTableAdapter контактыTableAdapter;
        private System.Windows.Forms.BindingSource контактыBindingSource;
        private System.Windows.Forms.DataGridView контактыDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выйтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem правкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьКонтактToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserID;
        private System.Windows.Forms.ToolStripMenuItem удалитьНапоминаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridView напоминанияDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.TextBox textBox1;
    }
}

